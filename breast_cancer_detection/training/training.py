import torch
from torch.utils.tensorboard import SummaryWriter
from tqdm import tqdm


def train_epoch(
    model,
    optimizer: torch.optim.Optimizer,
    data_loader: torch.utils.data.DataLoader,
    device: torch.device,
    epoch: int,
) -> float:
    """Train one epoch"""
    model.train()
    with tqdm(data_loader, unit="batch") as t_epoch:
        t_epoch.set_description(f"Epoch {epoch} (training)")
        running_loss = 0
        seen_images = 0
        for (images, targets) in t_epoch:
            images = list(image.to(device) for image in images)
            targets = [{k: v.to(device) for k, v in t.items()} for t in targets]

            # Feed the training samples to the model and compute the losses
            loss_dict = model(images, targets)
            losses = sum(loss for loss in loss_dict.values())
            loss_value = losses.item()

            # Pytorch function to initialize optimizer
            optimizer.zero_grad()
            # Compute gradients or the backpropagation
            losses.backward()
            # Update current gradient
            optimizer.step()

            running_loss += loss_value
            seen_images += len(images)
            t_epoch.set_postfix(loss=running_loss / seen_images)
    return running_loss / len(data_loader)


def validate_epoch(
    model,
    data_loader: torch.utils.data.DataLoader,
    device: torch.device,
    epoch: int,
) -> float:
    """Validate one epoch"""
    model.train()  # The model doesn't return its loss in eval mode
    with torch.no_grad():
        with tqdm(data_loader, unit="batch") as t_epoch:
            t_epoch.set_description(f"Epoch {epoch} (validation)")
            running_loss = 0
            seen_images = 0
            for (images, targets) in t_epoch:
                images = list(image.to(device) for image in images)
                targets = [{k: v.to(device) for k, v in t.items()} for t in targets]

                # Feed the validation samples to the model and compute the losses
                loss_dict = model(images, targets)
                losses = sum(loss for loss in loss_dict.values())
                loss_value = losses.item()

                running_loss += loss_value
                seen_images += len(images)

                t_epoch.set_postfix(loss=running_loss / seen_images)
        return running_loss / len(data_loader)


def training_main(
    model,
    optimizer: torch.optim.Optimizer,
    train_data_loader: torch.utils.data.DataLoader,
    val_data_loader: torch.utils.data.DataLoader,
    device: torch.device,
    nb_epochs: int,
    val_freq: int,
    writer: SummaryWriter,
):
    """Main training function"""
    for epoch in range(1, nb_epochs + 1):
        train_loss = train_epoch(model, optimizer, train_data_loader, device, epoch)
        writer.add_scalar("Loss/train", train_loss, epoch)
        if val_freq != 0 and epoch % int(val_freq) == 0:
            validate_loss = validate_epoch(model, val_data_loader, device, epoch)
            writer.add_scalar("Loss/val", validate_loss, epoch)
        torch.cuda.empty_cache()
    writer.flush()
