import albumentations as A
from albumentations.pytorch.transforms import ToTensorV2


def hflip_transform(p=0.5):
    return A.Compose(
        [
            A.HorizontalFlip(p=p),
            # ToTensorV2 converts image to pytorch tensor without div by 255
            ToTensorV2(p=1.0),
        ],
        bbox_params={"format": "pascal_voc", "label_fields": ["labels"]},
    )


def to_tensor():
    return A.Compose(
        [ToTensorV2(p=1.0)], bbox_params={"format": "pascal_voc", "label_fields": ["labels"]}
    )


def transform_pipeline(clahe_prob=1, noise_prob=0.5, hflip_prob=0.5, brightness_contrast_prob=0.2):
    return A.Compose(
        [
            A.augmentations.transforms.CLAHE(p=clahe_prob),
            A.augmentations.transforms.GaussNoise(p=noise_prob),
            A.HorizontalFlip(p=hflip_prob),
            A.RandomBrightnessContrast(p=brightness_contrast_prob),
            ToTensorV2(p=1.0),
        ],
        bbox_params={"format": "pascal_voc", "label_fields": ["labels"]},
    )
