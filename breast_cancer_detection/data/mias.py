from copy import copy
from dataclasses import dataclass
from enum import Enum
from pathlib import Path
from typing import Literal
from warnings import warn

import numpy as np
import sklearn as sk
import torch
from PIL import Image
from torch.utils.data import Dataset, Subset

from breast_cancer_detection.data.transforms import to_tensor


class LabelDetail(Enum):
    TUMOR = 0  # Simple bool classifier, tumor or no tumor
    TUMOR_MALIGNANT = 1  # Benign tumor, malignant tumor, or no tumor
    ABNORMALITY = 2  # Classify abnormality type
    ABNORMALITY_MALIGNANT = 3  # Classify abnormality type along with malignancy


def num_classes(label_detail: LabelDetail) -> int:
    """
    Get the number of box classes for a given level of label detail.
    """
    if label_detail == LabelDetail.TUMOR:
        return 3
    if label_detail == LabelDetail.TUMOR_MALIGNANT:
        return 4
    if label_detail == LabelDetail.ABNORMALITY:
        return 8
    if label_detail == LabelDetail.ABNORMALITY_MALIGNANT:
        return 14


@dataclass
class Box:
    """
    Data abound bounding boxes and the tissue they contain.

    :ivar abnormality: Class of abnormality present, None if the tissue is normal.
    :ivar is_malignant: Whether the abnormality is malignant. None if the tissue is normal.
    :ivar coords: Coordinates of the box in the Pascal VOC format (x_min, y_min, x_max, y_max).
    """

    abnormality: Literal["CALC", "CIRC", "SPIC", "MISC", "ARCH", "ASYM"] | None
    is_malignant: bool | None
    coords: tuple[float, float, float, float]

    @classmethod
    def from_parsed_text(cls, parsed_text: list[str]):
        abnormality = parsed_text[0]
        if abnormality == "NORM":
            abnormality = None
        # Parse malignant character and location
        if abnormality is not None:
            is_malignant = parsed_text[1] == "M"
            center = float(parsed_text[2]), 1024 - float(parsed_text[3])
            radius = float(parsed_text[4])
        else:  # Generate random bounding box
            is_malignant = None
            radius = cls.random_radius(50, 20)
            center = np.random.randint(radius, 1024 - radius, 2)
        coords = cls.radius2pascal_voc(*center, radius)
        return cls(abnormality=abnormality, is_malignant=is_malignant, coords=coords)

    @staticmethod
    def radius2pascal_voc(x: float, y: float, radius: float) -> tuple[float, float, float, float]:
        """
        Convert a center and radius to a rectangle in the Pascal VOC format.

        :return: A rectangle of coordinates (x_min, y_min, x_max, y_max)
        """
        return (x - radius, y - radius, x + radius, y + radius)

    @staticmethod
    def random_radius(mean: float, std: float) -> float:
        """ "
        Generate a random radius (>=1) following the normal law N(mean, std).
        """
        radius = int(np.floor(np.random.randn() * std + mean))
        return max(radius, 1)

    def get_label(self, label_detail: LabelDetail) -> int:  # pylint: disable=too-many-return-statements
        """
        Generate a numeric label associated to the image data.
        """
        match label_detail:
            case LabelDetail.TUMOR:
                if self.abnormality is None:
                    return 1
                return 2
            case LabelDetail.TUMOR_MALIGNANT:
                if self.abnormality is None:
                    return 1
                if self.is_malignant:
                    return 2
                return 3
            case LabelDetail.ABNORMALITY:
                return {None: 1, "CALC": 2, "CIRC": 3, "SPIC": 4, "MISC": 5, "ARCH": 6, "ASYM": 7}[
                    self.abnormality
                ]
            case LabelDetail.ABNORMALITY_MALIGNANT:
                label = {None: 1, "CALC": 2, "CIRC": 4, "SPIC": 6, "MISC": 8, "ARCH": 10, "ASYM": 12}[
                    self.abnormality
                ]
                if self.abnormality is not None:
                    label += int(self.is_malignant)
                return label


@dataclass
class ImageData:
    """
    Data about an image from the MIAS dataset.

    :ivar name: The name of the image.
    :ivar path: The path to the image.
    :ivar background_tissue: Character of background tissue.
    :ivar boxes: List of bounding boxes.

    """

    name: str
    path: Path
    background_tissue: Literal["F", "G", "D"]
    boxes: list[Box]

    @classmethod
    def from_text(cls, text: str, img_dir: Path):
        """
        Generate an ImageData instance from a line of text from the MIAS info file.

        :param text: A lign of text in one of the following formats:
         - mdb001 G CIRC B 535 425 197
         - mdb003 D NORM
        :param img_dir: The root of the images' directory.
        """
        parsed_text = text.split(" ")
        name = parsed_text[0]
        path = (img_dir / Path(name)).with_suffix(".pgm")
        box = Box.from_parsed_text(parsed_text[2:])
        return cls(name=name, path=path, background_tissue=parsed_text[1], boxes=[box])


class MIASDataset(Dataset):
    def __init__(
        self,
        info_file: Path | str,
        transforms=to_tensor(),
        label_detail: LabelDetail = LabelDetail.TUMOR,
    ):
        """
        :param info_file: Path to the Info.txt file.
        :param label_detail: How the data should be labelled.
        See LabelDetail documentation for more details.
        """
        self.info_file = Path(info_file)
        self.transforms = transforms
        self.label_detail = label_detail
        self.img_data: list[ImageData] = []
        self.name2id: dict[str, int] = {}

        with open(self.info_file, "r") as dataset:
            info = [line.strip() for line in dataset.readlines() if line.startswith("mdb")]

        error_lines = []
        idx = 0
        for i, txt in enumerate(info):
            try:
                line_data = ImageData.from_text(txt, img_dir=self.info_file.parent)
            except (IndexError, ValueError):
                error_lines.append((i, txt))
                continue
            if line_data.name not in self.name2id:
                # New mammogram
                self.img_data.append(line_data)
                self.name2id[line_data.name] = idx
                idx += 1
            else:
                # Old mammogram, add new bbox
                identifier = self.name2id[line_data.name]
                self.img_data[identifier].boxes.append(line_data.boxes[0])

        # Warning message in case of parsing errors
        if error_lines:
            warning_msg = (
                f"{len(error_lines)} lines couldn't be parsed and weren't added to the dataset:"
            )
            for line_num, line_txt in error_lines[:10]:
                warning_msg += f"\n • line {line_num}: {line_txt}"
            if len(error_lines) > 10:
                warning_msg += "\n... and more"
            warn(warning_msg)

    def _load_image(self, image_id: int) -> np.array:
        """
        Load an image from a file.
        """
        img = np.asarray(
            Image.open(self.img_data[image_id].path), dtype=np.uint8
        )  # Load grayscale image
        return np.stack((img,) * 3, axis=-1)  # Simulate rgb image by stacking 3 grayscale channels

    def __getitem__(self, image_id):
        img = self._load_image(image_id)
        data = self.img_data[image_id]

        # Convert everything to tensor
        image_id = torch.tensor([image_id])
        boxes = torch.as_tensor([box.coords for box in data.boxes], dtype=torch.float32)
        labels = torch.as_tensor(
            [box.get_label(self.label_detail) for box in data.boxes], dtype=torch.int64
        )

        # Compute the area of the bounding boxes
        area = (boxes[:, 3] - boxes[:, 1]) * (boxes[:, 2] - boxes[:, 0])

        # Use the COCO template for targets
        target = {
            "boxes": boxes,
            "labels": labels,
            "image_id": image_id,
            "area": area,
            "iscrowd": torch.as_tensor([0], dtype=torch.int64),
        }

        # Apply transforms
        if self.transforms:
            sample = self.transforms(image=img, bboxes=target["boxes"], labels=target["labels"])
            img = sample["image"]
            target["boxes"] = torch.tensor(sample["bboxes"])

        # Normalize from 0-255 to 0-1
        img = (img / 255.0).float()
        # return the image, the boxlist and the idx in your dataset
        return img, target

    def __len__(self):
        return len(self.img_data)

    def get_indices(self, img_names: list[str]) -> list[int]:
        """
        Return the indices associated to image names.

        :param img_names: A list containing image names, for instance ('mdb001', 'mdb002').
        """
        indices = [None] * len(img_names)
        for idx, image_data in enumerate(self.img_data):
            for i, name in enumerate(img_names):
                if image_data.path.stem == name:
                    indices[i] = idx
        return indices

    def split(
        self, val_img_names: list[str], test_img_names: list[str]
    ) -> tuple[Subset, Subset, Subset]:
        """
        Train/test split.
        """
        reused_imgs = set(val_img_names).intersection(test_img_names)
        if reused_imgs:
            raise ValueError(f"{reused_imgs} are both in validation and testing sets.")
        train_idx, val_idx, test_idx = [], [], []
        for idx, image_data in enumerate(self.img_data):
            img_name = image_data.path.stem
            if img_name in val_img_names:
                val_idx.append(idx)
            elif img_name in test_img_names:
                test_idx.append(idx)
            else:
                train_idx.append(idx)
        # TODO: figure out a cleaner way than copying
        return Subset(copy(self), train_idx), Subset(copy(self), val_idx), Subset(copy(self), test_idx)

    def stratified_split(
        self,
        val_size: float,
        test_size: float,
        label_detail: LabelDetail = None,
        random_state: int | None = None,
    ) -> tuple[Subset, Subset, Subset]:
        """
        Split a dataset so that classes are balanced.

        :param val_size: Proportion of the validation_data (in 0-1).
        :param test_size: Proportion of the testing data (in 0-1).
        :param label_detail: How the data should be labelled for stratification.
            Uses the value given to the dataset if set to None.
        :param label_detail: Controls the shuffling applied to the data before applying the split.
            Pass an int for reproducible output across multiple function calls.
        """
        if not (0 < test_size < 1 and 0 < val_size < 1):
            raise ValueError("Test and validation batch sizes are meant to be between 0 and 1.")

        if label_detail is None:
            label_detail = self.label_detail
        train_idx, val_idx, test_idx, trainval_idx = [], [], [], []
        # Separate full dataset between trainval and test
        all_data_ids = range(len(self))
        # Warning: labels are actually per box, not per image.
        # What follows works fine for MIAS as most images only have 1 box, but is anything but generic.
        all_data_labels = [self.img_data[i].boxes[0].get_label(label_detail) for i in all_data_ids]
        trainval_idx, test_idx, y_trainval, _ = sk.model_selection.train_test_split(
            all_data_ids,
            all_data_labels,
            test_size=test_size,
            stratify=all_data_labels,
            random_state=random_state,
        )
        # Separate trainval images as train and val
        train_idx, val_idx, _, _ = sk.model_selection.train_test_split(
            trainval_idx,
            y_trainval,
            test_size=val_size / (1 - test_size),
            stratify=y_trainval,
            random_state=random_state,
        )
        # TODO: figure out a cleaner way than copying
        return Subset(copy(self), train_idx), Subset(copy(self), val_idx), Subset(copy(self), test_idx)


def mean_std(dataset: Dataset) -> tuple[torch.Tensor, torch.Tensor]:
    """
    Compute the mean and std values per channel on an image dataset.

    :return: ([mean_r, mean_g, mean_b], [std_r, std_g, std_b])
    """
    channels_sum, channels_sqrd_sum = 0, 0

    for img, _ in dataset:
        channels_sum += img.mean(dim=[1, 2])
        channels_sqrd_sum += (img**2).mean(dim=[1, 2])

    mean = channels_sum / len(dataset)
    std = (channels_sqrd_sum / len(dataset) - mean**2) ** 0.5

    return mean, std
