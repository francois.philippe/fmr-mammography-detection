import hashlib
import tarfile
from pathlib import Path
from typing import Optional
from urllib.request import urlopen

from tqdm import tqdm


def download_file(
    dest_file: Path, url: str, show_progress: bool = True, chunk_size: int = 1024 * 1024
) -> Path:
    """
    Download the file located at url to dest_file.

    Will follow redirects.
    :param dest_file: The file destination path.
    :param url: A link to the remote file.
    :param show_progress: Print a progress bar to stdout if set to True.
    :param chunk_size: Number of bits to download before saving stream to file.

    ;return: The file the dataset was downloaded to.
    """
    # Ensure the directory we'll put the downloaded file in actually exists
    dest_file.parent.mkdir(parents=True, exist_ok=True)
    with urlopen(url) as response:
        file_size = int(response.headers["Content-Length"])
        with open(dest_file, "wb") as db_file:
            with tqdm(
                total=file_size, unit="B", unit_scale=True, disable=not show_progress
            ) as progress_bar:
                while chunk := response.read(chunk_size):
                    db_file.write(chunk)
                    progress_bar.update(chunk_size)
        return dest_file


def checksum(file: Path) -> bytes:
    """
    Compute the md5 checksum of a file.
    """
    hasher = hashlib.md5()
    with open(file, "rb") as f:
        while chunk := f.read(128 * hasher.block_size):
            hasher.update(chunk)
    return hasher.digest()


def download_tar_dataset(dest_dir: Path | str, db_url: str, db_hash: Optional[bytes] = None) -> None:
    """
    Download and extract a tarfile dataset.

    :param dest_dir: The extracted dataset destination directory.
    :param db_url: The remote location of the compressed dataset.
    :param db_hash: The compressed dataset's md5 hash.
    If set to None, the downloaded file won't be checked for corruption.
    """

    dest_dir = Path(dest_dir)
    temp_db = download_file(Path(dest_dir / "temp_db"), db_url)
    if db_hash is not None and checksum(temp_db) != db_hash:
        raise ValueError("File corrupted during download.")
    with tarfile.open(temp_db) as tar:
        tar.extractall(dest_dir)
        temp_db.unlink()
