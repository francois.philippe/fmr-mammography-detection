import torch
from torchvision.ops import nms

Prediction = list[dict[str, torch.Tensor]]


def non_maximum_suppression(predictions: Prediction, iou_threshold: float) -> Prediction:
    """
    Apply non maximum suppression to predictions.
    """
    nms_prediction = []
    for pred in predictions:
        boxes = pred["boxes"].cpu()
        scores = pred["scores"].cpu()
        labels = pred["labels"].cpu()
        temp = nms(
            boxes, scores, iou_threshold
        )  # int64 tensor with the indices of the elements that have been kept by NMS,
        # sorted in decreasing order of scores
        nms_prediction.append(
            {
                "boxes": boxes[temp],
                "scores": scores[temp],
                "labels": labels[temp],
            }
        )
    return nms_prediction


def confidence_filter(predictions: Prediction, conf_thresh: float) -> Prediction:
    """
    Filter out low confidence boxes from predictions.
    """
    filtered_predictions = []
    for pred in predictions:
        boxes = pred["boxes"].cpu()
        labels = pred["labels"].cpu()
        scores = pred["scores"].cpu()
        mask = scores >= conf_thresh
        filtered_predictions.append(
            {
                "boxes": boxes[mask],
                "labels": labels[mask],
                "scores": scores[mask],
            }
        )
    return filtered_predictions
