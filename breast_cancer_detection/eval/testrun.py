from typing import Any

import torch
from torch.utils.data import DataLoader, Dataset
from torchvision.models.detection.faster_rcnn import FasterRCNN

from breast_cancer_detection.eval.evaluate import evaluate_main
from breast_cancer_detection.eval.filters import confidence_filter, non_maximum_suppression
from breast_cancer_detection.eval.utils import compute_f1_light


def testrun(
    model: FasterRCNN,
    test_set: Dataset,
    data_loader: DataLoader,
    nms_iou_threshold: float,
    true_positive_iou_threshold: float,
    device: torch.device | None = None,
    seed=0,
) -> tuple[dict[str, Any], dict[str, Any]]:
    """
    Get metrics for test set.

    :param model: The trained model.
    :param test_set: The data the modle should be evaluated on.
    :param data_loader: The data loader associated to the test set.
    :param nms_iou_threshold: Threshold for IoU in the NMS algorithm.
    :param true_positive_iou_threshold: IoU with ground truth over which
    a box is labelled as a true positive.
    :param device: Device on which to run the inference. Set to None to autodetect.
    :param seed: Random seed for reproductibility.

    """
    # Generate predictions
    torch.manual_seed(seed)
    model.eval()
    if device is None:
        device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
    model.to(device)
    predictions = []
    with torch.no_grad():
        for img, _ in data_loader:
            img_device = [x.to(device) for x in img]
            predictions.extend(model(img_device))
    # Apply confidence filter
    predictions = confidence_filter(predictions, 0.1)
    # Apply nms
    predictions = non_maximum_suppression(predictions, nms_iou_threshold)
    # Evaluate metrics (results contains one dictionary by image with key
    # metrics and infos on target/prediction boxes)
    f1_score, _ = evaluate_main(test_set, predictions, true_positive_iou_threshold)
    f1_score_light = compute_f1_light(test_set, predictions)
    return (f1_score, f1_score_light)
