import torch
from torchvision.models.detection.faster_rcnn import FasterRCNN


def predict(
    model: FasterRCNN,
    images: torch.utils.data.Dataset | list[torch.Tensor],
    indexes: list[int] | None = None,
    device: torch.device | None = None,
) -> list[dict]:
    """
    Generate predictions.

    :param images: images to run the prediction on.


    """
    if indexes is None:
        indexes = range(len(images))
    if device is None:
        device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
    # Extract input images.
    img = [images[x][0].to(device) for x in indexes]
    # Switch the model to evaluation mode.
    model.eval()
    # Compute the predictions.
    with torch.no_grad():
        predictions = model(img)
    return predictions
