import torch
from torch.utils.data import Subset

from breast_cancer_detection.eval.filters import Prediction
from breast_cancer_detection.eval.utils import compute_f1_score_global, compute_iou

Results = dict[str, dict[str, torch.Tensor]]


def evaluate_main(
    subset: torch.utils.data.Dataset,
    nms_predictions: Prediction,
    true_positive_iou_threshold: float,
) -> float | Results:
    """Main evaluation function"""

    # Define empty results dictionary
    results = {}

    for i in range(len(subset)):  # pylint: disable=consider-using-enumerate
        target = subset[i][1]
        # Treat prediction (NMS, viz, metrics)
        if isinstance(subset, Subset):
            img_name = subset.dataset.img_data[subset.indices[i]].name
        else:
            img_name = subset.dataset.img_data[i].name
        results[img_name] = {"id_in_subset": i, "prediction": nms_predictions[i]}
        evaluate_true_positives_img(
            nms_predictions[i], target, results[img_name], true_positive_iou_threshold
        )
        evaluate_metrics_img(results[img_name])
    return compute_f1_score_global(results), results


def evaluate_true_positives_img(
    prediction: dict,
    target: dict[str, torch.tensor],
    results_img: dict[str, torch.Tensor],
    true_positive_threshold: float,
) -> None:
    """
    Evaluate true positives.
    - Determine if a prediction is a true positive
    - Determine fit between target box and prediction box

    results_img is modified
    """

    # Create new values in results to store if a prediction box is a true positive or not,
    # and if yes of which target box
    results_img["target"] = {"boxes": target["boxes"], "labels": target["labels"]}
    nb_prediction_boxes = len(results_img["prediction"]["boxes"])
    nb_target_boxes = len(results_img["target"]["boxes"])
    results_img["prediction"]["is_true_positive"] = [None] * nb_prediction_boxes
    results_img["prediction"]["target_box"] = [None] * nb_prediction_boxes
    results_img["prediction"]["best_iou"] = [0] * nb_prediction_boxes
    results_img["target"]["is_detected"] = [False] * nb_target_boxes
    results_img["target"]["best_iou"] = [0] * nb_target_boxes
    results_img["target"]["prediction_box"] = [None] * nb_target_boxes
    # For each ground truth box (in target)
    for i in range(len(target["boxes"])):
        label_target = target["labels"][i]
        # For each prediction box
        for j in range(len(prediction["boxes"])):
            if label_target == prediction["labels"][j] and label_target != 1:  # If the label is good
                iou = compute_iou(prediction["boxes"][j], target["boxes"][i])  # Compute iou
                if iou >= results_img["prediction"]["best_iou"][j] and iou >= true_positive_threshold:
                    # Prediction box is a true positive and the considered target box is the one
                    # it fits best so far. Store that the box is a true positive and the target is
                    # detected
                    results_img["prediction"]["is_true_positive"][j] = True
                    results_img["target"]["is_detected"][i] = True
                    # Store that the target box is the best for the prediction box so far
                    results_img["prediction"]["target_box"][j] = i
                    results_img["prediction"]["best_iou"][j] = iou
                    # Check if this is the best prediction box for the target box so far
                    if iou >= results_img["target"]["best_iou"][i]:
                        results_img["target"]["best_iou"][i] = iou
                        results_img["target"]["prediction_box"][i] = j
            elif prediction["labels"][j] != 1:
                if results_img["prediction"]["is_true_positive"][j] is None:
                    results_img["prediction"]["is_true_positive"][j] = False


def evaluate_metrics_img(results_img: dict[str, torch.Tensor]) -> None:
    """Compute metrics
    results_img is modified"""
    # Compute number of false positives
    results_img["nb_false_positives"] = results_img["prediction"]["is_true_positive"].count(False)
    results_img["nb_true_positives"] = results_img["prediction"]["is_true_positive"].count(True)
    # Compute number of undetected boxes
    results_img["nb_undetected_boxes"] = results_img["target"]["is_detected"].count(False)
    results_img["nb_detected_boxes"] = results_img["target"]["is_detected"].count(True)
    # Compute number of total target boxes we should have found
    labels = results_img["target"]["labels"]
    mask = labels != 1
    results_img["nb_true_target_boxes"] = mask.sum().item()
