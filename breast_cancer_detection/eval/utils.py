import torch

Results = dict[str, dict[str, torch.Tensor]]
Prediction = list[dict[str, torch.Tensor]]


def compute_area(box: torch.tensor) -> float:
    """Compute area of box (xmin,ymin,xmax,ymax)"""
    return (box[3] - box[1]) * (box[2] - box[0])


def compute_iou(box_a: torch.tensor, box_b: torch.tensor) -> float:
    """Compute IoU between two different boxes"""
    # Compute area of box A
    area_a = compute_area(box_a)
    # Compute area of box B
    area_b = compute_area(box_b)
    # Compute intersection and its area
    box_intersection = [
        max(box_a[0], box_b[0]),
        max(box_a[1], box_b[1]),
        min(box_a[2], box_b[2]),
        min(box_a[3], box_b[3]),
    ]
    area_intersection = compute_area(box_intersection)
    # Compute IoU (Intersection over Union)
    iou_score = (area_intersection) / (area_a + area_b - area_intersection)
    return iou_score


def compute_f1_score_global(results: Results):
    total_true_positives = 0
    total_predicted_boxes = 0
    total_target_boxes = 0
    total_undetected_targets = 0
    total_false_positives = 0
    for result_img in results.values():
        nb_true_target_boxes_img = result_img["nb_true_target_boxes"]
        nb_detected_boxes_img = result_img["nb_detected_boxes"]
        nb_false_positives_img = result_img["nb_false_positives"]
        # Number of filtered true positives
        nb_filtered_true_positives_img = nb_detected_boxes_img
        # Number of filtered prediction boxes
        nb_filtered_prediction_boxes_img = nb_detected_boxes_img + nb_false_positives_img
        # F1 score
        total_true_positives += nb_filtered_true_positives_img
        total_predicted_boxes += nb_filtered_prediction_boxes_img
        total_target_boxes += nb_true_target_boxes_img
        total_undetected_targets += nb_true_target_boxes_img - nb_filtered_true_positives_img
        total_false_positives += nb_false_positives_img
    recall = total_true_positives / total_predicted_boxes
    precision = total_true_positives / total_target_boxes
    f1_score = 2 * precision * recall / (precision + recall)
    return {
        "f1_score": f1_score,
        "precision": precision,
        "recall": recall,
        "true_positives": total_true_positives,
        "undetected_targets": total_undetected_targets,
        "false_positives": total_false_positives,
        "total_targets": total_target_boxes,
    }


def compute_f1_light(subset: torch.utils.data.Dataset, nms_predictions: Prediction):
    nb_true_pos = 0
    nb_false_pos = 0
    nb_undetected = 0
    for img_idx in range(len(nms_predictions)):  # pylint: disable=consider-using-enumerate
        target = subset[img_idx][1]
        if (target["labels"] == 2).any() and (nms_predictions[img_idx]["labels"] == 2).any():
            nb_true_pos += 1
        elif (target["labels"] == 2).any():
            nb_undetected += 1
        elif (nms_predictions[img_idx]["labels"] == 2).any():
            nb_false_pos += 1
        else:
            nb_true_pos += 1
    recall = nb_true_pos / (nb_true_pos + nb_false_pos)
    precision = nb_true_pos / (nb_true_pos + nb_undetected)
    f1_score = 2 * precision * recall / (precision + recall)
    return {
        "f1_light": f1_score,
        "recall_light": recall,
        "precision_light": precision,
        "true_pos_light": nb_true_pos,
        "false_pos_light": nb_false_pos,
        "undetected_light": nb_undetected,
    }
