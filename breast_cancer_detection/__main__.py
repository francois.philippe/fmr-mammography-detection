import click

from breast_cancer_detection.data import download_tar_dataset


def download_mias() -> None:
    """
    Download the MIAS dataset and save it to the /data directory.
    """
    print("Downloading MIAS dataset...")
    download_tar_dataset(
        dest_dir="data/mias",
        db_url="http://peipa.essex.ac.uk/pix/mias/all-mias.tar.gz",
        db_hash=b"\xe2\x8dfU\t\xbf\xa7\x82\x9b\xe4X\xba\xd8\xf2FB",
    )
    print("MIAS dataset extracted to /data/mias")


@click.command()
@click.option("--download", is_flag=True, help="Download the MIAS dataset.")
def cli(download):
    """Offers the option to download MIAS"""
    if download:
        download_mias()


if __name__ == "__main__":
    cli()  # pylint: disable = no-value-for-parameter
