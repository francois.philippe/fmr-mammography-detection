import matplotlib.pyplot as plt
import torch
from matplotlib import patches


def plot_boxes(
    img,
    target: dict,
    predicted: dict | None = None,
    width=2,
    edgecolor_targ="#99b83c",
    edgecolor_pred="red",
):
    # Create figure and axes
    _, ax = plt.subplots()

    # Display the image
    ax.imshow(img.moveaxis(0, 2))

    for (x_min, y_min, x_max, y_max) in target["boxes"]:
        # Create a Rectangle patch
        rect = patches.Rectangle(
            (x_min, y_min),
            x_max - x_min,
            y_max - y_min,
            linewidth=width,
            edgecolor=edgecolor_targ,
            facecolor="none",
        )

        # Add the patch to the Axes
        ax.add_patch(rect)

    if predicted is not None:
        for (x_min, y_min, x_max, y_max) in predicted["boxes"]:
            # Create a Rectangle patch
            rect = patches.Rectangle(
                (x_min, y_min),
                x_max - x_min,
                y_max - y_min,
                linewidth=width,
                edgecolor=edgecolor_pred,
                facecolor="none",
            )

            # Add the patch to the Axes
            ax.add_patch(rect)
    plt.show()


Prediction = list[dict[str, torch.Tensor]]


def easy_plot(
    image_set: torch.utils.data.Dataset | list[torch.Tensor],
    predictions: Prediction,
    idx_to_plot: int,
    target: dict | None = None,
    width=2,
    edgecolor_targ="#99b83c",
    edgecolor_pred="red",
):
    if target is None:
        target = image_set[idx_to_plot][1]

    # target filter
    t_labels = target["labels"].cpu()
    mask = t_labels != 1
    t_boxes = target["boxes"].cpu()[mask]

    # predicted filter
    p_labels = predictions[idx_to_plot]["labels"].cpu()
    mask = p_labels != 1
    p_boxes = predictions[idx_to_plot]["boxes"].cpu()[mask]

    return plot_boxes(
        image_set[idx_to_plot][0],
        {"boxes": t_boxes},
        {"boxes": p_boxes},
        width,
        edgecolor_targ,
        edgecolor_pred,
    )
