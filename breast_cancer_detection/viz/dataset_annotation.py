import numpy as np
from PIL import Image

with open("/Users/AlixChazottes/Desktop/fmr-mammo-data/annotations.txt") as f:
    annotations = f.readlines()

mammos = {}
for annotation in annotations:
    annotation = annotation.split()
    if len(annotation) == 7:
        n = annotation[0][3:]
        coords = [annotation[3], int(annotation[4]), int(annotation[5]), int(annotation[6])]
        if n in mammos:
            mammos[n].append(coords)
        else:
            mammos[n] = [coords]
    else:
        continue

for n in mammos.items():
    image = Image.open(f"/Users/AlixChazottes/Desktop/fmr-mammo-data/all-mias/mdb{n}.pgm")
    image = image.convert("RGB")
    image_as_array = np.copy(np.array(image))
    for lesion in mammos[n]:
        isbenign = lesion[0] == "B"
        color = [0, 128, 0] if isbenign else [255, 0, 0]
        cy = lesion[1]
        cx = lesion[2]
        r = lesion[3]

        for x in range(cx - r, cx + r + 1):
            for y in range(cy - r, cy + r + 1):
                # if (r - 2) ** 2 <= (x - cx) ** 2 + (y - cy) ** 2 <= (r + 2) ** 2: # CERCLE MOCHE
                if x == cx - r or x == cx + r or y == cy - r or y == cy + r:
                    image_as_array[1024 - x, y] = color

    pilImage = Image.fromarray(image_as_array)
    pilImage.save(f"/Users/AlixChazottes/Desktop/fmr-mammo-data/annotated_rv/annotated_{n}.png")
