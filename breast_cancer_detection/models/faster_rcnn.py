from typing import Literal

from torchvision.models.detection import fasterrcnn_resnet50_fpn, fasterrcnn_resnet50_fpn_v2
from torchvision.models.detection.faster_rcnn import FasterRCNN, FastRCNNPredictor


def faster_rcnn(num_classes: int, version: Literal[1, 2], pretrained: bool, **kwargs) -> FasterRCNN:
    """
    Build a Faster-RCNN model.
    """
    if version not in (1, 2):
        raise ValueError("Version must be 1 or 2.")

    weights = "COCO_V1" if pretrained else None
    if version == 1:
        model = fasterrcnn_resnet50_fpn(weights=weights, **kwargs)
    if version == 2:
        model = fasterrcnn_resnet50_fpn_v2(weights=weights, **kwargs)
    # Replace the pre-trained head with a new one
    in_features = model.roi_heads.box_predictor.cls_score.in_features
    model.roi_heads.box_predictor = FastRCNNPredictor(in_features, num_classes)
    return model
